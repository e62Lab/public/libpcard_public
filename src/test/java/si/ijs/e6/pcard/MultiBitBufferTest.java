package si.ijs.e6.pcard;

import static org.junit.Assert.*;

/**
 * Created by matjaz on 10/17/18.
 */
public class MultiBitBufferTest {
    byte[] byteBuffer = new byte[2000];
    MultiBitBuffer buffer = new MultiBitBuffer(byteBuffer);

    void initByteBuffer(int starting, int addition) {
        byte val = (byte)starting;
        for (int i = 0; i < byteBuffer.length; ++i) {
            byteBuffer[i] = val;
            val += addition;
        }
    }

    @org.junit.Test
    public void getInt() throws Exception {
        initByteBuffer(1, 3);
        int val4 = buffer.getUnsignedInt(8, 8);
        assertEquals(4, val4);
    }

    @org.junit.Test
    public void setInts() throws Exception {
        initByteBuffer(0, 0);
        buffer.setInts(0xFF, 0, 8, 1);
        assertEquals((byte)0xFF, byteBuffer[0]);
    }

    @org.junit.Test
    public void setAndGet() throws Exception {
        final int bitLen = 9;
        final int bitMask = (1 << bitLen)-1;
        for (int ofs = 0; (ofs+bitLen) < 8*byteBuffer.length; ofs+=bitLen)
            buffer.setInts(ofs, ofs, bitLen, 1);

        for (int ofs = 0; (ofs+bitLen) < 8*byteBuffer.length; ofs+=bitLen)
            assertEquals(ofs & bitMask, buffer.getUnsignedInt(ofs, bitLen) & bitMask);
    }

    @org.junit.Test
    public void generateNormalPcardPacket() throws Exception {
        //////////////////////////////////////////////////////////////////////////////////////
        // pack the data
        byte[] bb = new byte[19];
        MultiBitBuffer mbb = new MultiBitBuffer(bb);

        int destIndex = 0;
        // expand the samples
        for (int i = 0; i < 14; ++i) {
            int val = i*11;     // just some unique value
            mbb.setInts(val, i * 10, 10, 1);
            destIndex += 10;
        }

        // att the counter (max value)
        mbb.setInts(1023, destIndex, 10, 1);
        destIndex += 10;
        // add normal stream identifier
        mbb.setInts(0, destIndex,2, 1);
        destIndex += 2;

        assertEquals(19*8, destIndex);

        //////////////////////////////////////////////////////////////////////////////////////
        // unpack the data
        MultiBitBuffer umbb = new MultiBitBuffer(bb);
        int sourceIndex = 0;
        // expand the samples
        for (int i = 0; i < 14; ++i) {
            int val = mbb.getUnsignedInt(sourceIndex, 10);
            assertEquals(11*i, val);
            sourceIndex += 10;
        }

        // expand the counter (aka time) from the end of bitstream
        int counter = mbb.getUnsignedInt(14 * 10, 10);
        assertEquals(1023, counter);
    }
}