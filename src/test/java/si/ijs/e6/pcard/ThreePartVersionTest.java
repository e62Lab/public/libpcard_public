package si.ijs.e6.pcard;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by matjaz on 10/17/18.
 */
public class ThreePartVersionTest {
    String rawVersionString = "1.2.3";
    ThreePartVersion version = new ThreePartVersion(rawVersionString);

    @Test
    public void testToString() throws Exception {
        // toString should produce the same string that was parsed to produce class instance in the first place
        assertTrue(version.toString().equals(rawVersionString));
    }

    @Test
    public void getRawString() throws Exception {
        // raw revision should equal the string that class instance was initialized with
        assertTrue(version.getRawString().equals(rawVersionString));
    }

    @Test
    public void isProperlyFormed() throws Exception {
        assertTrue(version.isProperlyFormed());
        // non numeric characters are removed when parsing
        ThreePartVersion v10 = new ThreePartVersion("1.2a");
        assertTrue(v10.isProperlyFormed());
        assertTrue(v10.toString().equals("1.2.0"));
        // too many dots will cause an exception
        assertFalse(new ThreePartVersion("1.2.3.4").isProperlyFormed());
        // missing numbers between dots will cause an exception
        assertFalse(new ThreePartVersion("1..").isProperlyFormed());
        assertFalse(new ThreePartVersion("a.b.c").isProperlyFormed());
        // commas will be filtered out
        ThreePartVersion v20 = new ThreePartVersion("1,2,3");
        assertTrue(v20.isProperlyFormed());
        assertTrue(v20.toString().equals("123.0.0"));
    }

    @Test
    public void minVersion() throws Exception {
        ThreePartVersion v = new ThreePartVersion("3.4.5");
        // versions below or equal
        assertTrue(v.minVersion(3, 4, 5));
        assertTrue(v.minVersion(2, 4, 5));
        assertTrue(v.minVersion(3, 3, 5));
        assertTrue(v.minVersion(3, 4, 4));
        // versions above
        assertFalse(v.minVersion(3, 4, 6));
        assertFalse(v.minVersion(3, 5, 1));
        assertFalse(v.minVersion(4, 1, 1));
    }

}