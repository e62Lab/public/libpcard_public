package si.ijs.e6.pcard;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

class PatientMetaTest {

    @Test
    void birthdayFromString() {
        PatientMeta p = new PatientMeta();
        String date = "1912-09-10";
        long t = p.millisecondTimestampFromString(date);
        String date2 = p.millisecondTimestampAsString(t);

        System.out.println(("Dates: "+date+" : "+t+" : "+date2));
        Assert.assertTrue(date2.equals(date));
    }
}