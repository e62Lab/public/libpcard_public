package si.ijs.e6.pcard;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DataStreamProtocolTest {

    /**
     * Test packing and unpacking of normal data packets.
     *
     * To keep things simple, sample values will get incremental values equal to counter
     */
    @Test
    void packData() {
        BleDataStreamType streamType = BleDataStreamType.NormalStream;
        int values[] = new int[streamType.numSamplesPerPacket];

        // prepare a data stream for unpacking data
        DataStreamProtocol.DataStream dataStream = DataStreamProtocol.createDataStream(streamType);

        double lastTimestampInSeconds = 0;
        for (int i = 0; i < 128000; ++i) {
            values[i % streamType.numSamplesPerPacket] = i;
            // on the final sample of the packet, pack the packet
            if (i % streamType.numSamplesPerPacket == (streamType.numSamplesPerPacket - 1)) {
                long counter = (long)values[0];
                RawDataStreamPacket packet = DataStreamProtocol.packData((long)(1.0e9 / 128 * i), counter, values, streamType);

                // unpack the packet and confirm its contents
                assertEquals((long)(1.0e9 / 128 * i), packet.receivedNanoTime);
                UnpackedEcgData unpacked = dataStream.unpack(packet);
                assertEquals((long)(1.0e9 / 128 * i), unpacked.times[0]);
                lastTimestampInSeconds = unpacked.times[0]*1e-9;

                assertEquals(counter, unpacked.sampleCounter);
                assertEquals(counter % 1024, unpacked.rawSampleCounter);
                for (int j = 0; j < streamType.numSamplesPerPacket; ++j) {
                    assertEquals((counter+j) & 0x3FF, unpacked.values[j]);
                }
            }
        }
        // at the end, close to 1000 seconds must have passed
        assertTrue(Math.abs(lastTimestampInSeconds - 1000) < (14.0 / 128));
        System.out.println(lastTimestampInSeconds);
    }
}