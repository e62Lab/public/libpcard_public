package si.ijs.e6.pcard;


/**
 * Define stream type enum to be used in functions, but also define some properties for each type
 *
 * Data stream is composed of packets, which have uniform format, and store samples and counters,
 * the latter are used to align samples in a time series.
 *
 */
public enum BleDataStreamType {
    NormalStream("Normal ECG data stream", 14, 10, 14),             // the default used by Savvy/PCARD sensors
    ExtendedStream_0("Extended ECG data stream #1", 12, 24, 1),     // the immediate future
    Invalid("Invalid ECG data stream type", 0, 0, 0),               // not used yet  type 0(cannot reliably discern from undefined)
    AccelerometerStreamType0(20, "Accelerometer data stream type 0", 3, 6, 8, 14, 1),             // the default used by Savvy/PCARD sensors
    // the following one must be the last one to be defined:
    Undefined("Undefined/uninitialized data stream", 1, 0 ,0);  // This should be the default value

    /**
     * Short human readable description of the stream type.
     */
    public String description;
    /**
     * Number of ECG samples per packet.
     */
    public int numSamplesPerPacket;
    /**
     * The bit-width of individual samples.
     */
    public int sampleBitWidth = 10;
    /**
     * The bit-width of the counter in the packet.
     */
    public int perPacketCounterBitWidth;
    /**
     * The increase of the counter with each packet.
     */
    public int perPacketCounterIncrement;
    /**
     * The packet byte size; fixed for now as it is the same for all protocols.
     */
    public int packetByteSize = 19;
    /**
     * Number of channels each sample holds (1 for ECG, 3 for acceleration - x,y,z axis)
     */
    public int numChannels = 1;

    /**
     * Basic constructor
     * @param numSamplesPerPacket Number of ECG samples per single stream packet
     * @param perPacketCounterBitWidth     bit-width of counters used for time alignment
     * @param perPacketCounterIncrement    counter increment between packets (e.g. 14 or 1 or ...)
     */
    BleDataStreamType(String description, int numSamplesPerPacket, int perPacketCounterBitWidth, int perPacketCounterIncrement) {
        this.description = description;
        this.numSamplesPerPacket = numSamplesPerPacket;
        this.perPacketCounterBitWidth = perPacketCounterBitWidth;
        this.perPacketCounterIncrement = perPacketCounterIncrement;
        assertContentsFit();
    }

    BleDataStreamType(int packetSize, String description, int numChannels, int numSamplesPerPacket, int sampleBitWidth, int perPacketCounterBitWidth, int perPacketCounterIncrement) {
        this.packetByteSize = packetSize;
        this.description = description;
        this.numChannels = numChannels;
        this.numSamplesPerPacket = numSamplesPerPacket;
        this.sampleBitWidth = sampleBitWidth;
        this.perPacketCounterBitWidth = perPacketCounterBitWidth;
        this.perPacketCounterIncrement = perPacketCounterIncrement;
        assertContentsFit();
    }

    private void assertContentsFit() {
        // since asserts are not available on Android, this check here is in if statement an will always perform
        if ( ((packetByteSize * 8) <=
                (numChannels * numSamplesPerPacket * sampleBitWidth + perPacketCounterBitWidth)))
            throw new AssertionError("Error in properties of BleDataStreamType: contents do not fit into the specified packet size");
    }

    /**
     * Get the number of bits used to store a single ECG sample
     * @return bit width
     */
    public int getSampleBitWidth() {
        return sampleBitWidth;
    }

    /**
     * Get the bit width of counters
     * @return bit width
     */
    public int getPerPacketCounterBitWidth() {
        return perPacketCounterBitWidth;
    }

    /**
     * Get the increment value for counter between succeeding packets.
     * @return the increment value
     */
    public int getPerPacketCounterIncrement() {
        return perPacketCounterIncrement;
    }
}
