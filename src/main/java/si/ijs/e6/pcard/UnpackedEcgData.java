package si.ijs.e6.pcard;

/**
 * Created by matjaz on 7/28/15.
 */

/**
 * This class serves as a generic structure for holding a single packet (usually from BLE or s2 file)
 * of ECG data. Can be unpacked from from all types of data streaming packets. See
 * DataStreamUnpackers
 */
public class UnpackedEcgData {
    /**
     * How many samples have been there since sampling started (in some cases, its value is estimated)
     */
    public long sampleCounter;
    /**
     * The raw value of sample counter - as read from the packed ECG data.
     */
    public int rawSampleCounter;
    /**
     * Flag that the sample counter value could be wrong; e.g. for normal ECG data stream, this flag
     * is true if more than one 1024 samples seem to be skipped without {@link #discontinuedSampling}
     * flag.
     */
    public boolean sampleCounterUnsure = false;
    /**
     * Flag that sampling has been discontinued between the last sample of previous packet and first
     * sample given here (for normal stream, the {@link #sampleCounter} is made up, for others it
     * may have been restarted)
     */
    public boolean discontinuedSampling = false;
    /**
     * Sample times; the length of array  could be lower than the number of samples, e.g. only the
     * time for the first sample might be given.
     */
    public long[] times;
    /**
     * Sample values in raw form
     */
    public int[] values;
}
