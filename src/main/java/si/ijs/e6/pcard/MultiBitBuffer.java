package si.ijs.e6.pcard;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * A helper for filling byte[] with arbitrary data, much like Java.nio.ByteBuffer but allows for
 * irregular bit-sizes of elements. Little endian is assumed for all functions.
 */
public class MultiBitBuffer {
    private ByteBuffer bb;

    /**
     * Construct a multi bit buffer that wraps the given array of bytes.
     * @param data the array to wrap around
     */
    public MultiBitBuffer(byte[] data) {
        bb = ByteBuffer.wrap(data);
        bb.order(ByteOrder.LITTLE_ENDIAN);
    }

    /**
     * Get a short out of the buffer, starting at given bit-index address
     * @param bitIndex bit-index address (offset from start of buffer)
     * @param bitWidth the width (in bits) of the short stored in buffer
     * @return the value as short
     */
    public short getShort(int bitIndex, int bitWidth) {
        return (short) getUnsignedInt(bitIndex, bitWidth);
    }

    /**
     * Get a byte out of the buffer, starting at given bit-index address
     * @param bitIndex bit-index address (offset from start of buffer)
     * @param bitWidth the width (in bits) of the byte stored in buffer
     * @return the value as byte
     */
    public short getByte(int bitIndex, int bitWidth) {
        return (byte) getUnsignedInt(bitIndex, bitWidth);
    }

    /**
     * Get an unsigned int out of the buffer, starting at given bit-index address (0-based count)
     *
     * @param bitIndex bit-index address (offset from start of buffer)
     * @param bitWidth the width (in bits) of the int stored in buffer
     * @return the value as unsigned int
     */
    public int getUnsignedInt(int bitIndex, int bitWidth) {
        int byteOffset = bitIndex >> 3;
        int bitOffset = bitIndex & 7;
        int countBits = 0;

        int ret = 0;
        while (countBits < bitWidth) {
            // readyBits: bitWidth that can be from the byte at the current bitIndex
            int readyBits = 8 - bitOffset;
            // copy all remaining bitWidth in the byte?
            if (bitWidth - countBits >= readyBits) {
                int getBits = (bb.get(byteOffset) & 0xFF) >>> bitOffset;
                ret += (getBits << countBits);
                countBits += readyBits;
                byteOffset++;
                bitOffset = 0;
            } else {
                readyBits = (bitWidth - countBits);
                int getBits = (((bb.get(byteOffset) & 0xFF) >>> bitOffset) & ((1 << readyBits) - 1));
                ret += (getBits << countBits);
                countBits += readyBits;
                break;
            }
        }
        return ret;
    }

    /**
     * Get a signed int out of the buffer, starting at given bit-index address (0-based count)
     *
     * Warning, up to version 1.0.0, this function returned unsigned int!
     *
     * @param bitIndex bit-index address (offset from start of buffer)
     * @param bitWidth the width (in bits) of the int stored in buffer
     * @return the value as a signed int
     */
    public int getInt(int bitIndex, int bitWidth) {
        int ret = getUnsignedInt(bitIndex, bitWidth);
        // check the 'sign bit' and expand it

        return (ret << (32-bitWidth)) >> (32-bitWidth);
    }

    /**
     * Set the given number of bits to mirror the same number of bits provided with the integer variable
     * @param val        input bits; only bits 0 - (bits-1) of the provided value will be used
     * @param bitIndex   0-based index in the buffer, to receive the first bit
     * @param bitWidth   number of bits to write
     * @param numRepeats number of repeats (offset is added the value of bits written between iterations)
     *
     * setInts(x, 0, 10, 3) is equivalent to
     * setInts(x, 0, 10, 1); setInts(x, 10, 10, 1); setInts(x, 20, 10, 1);
     *
     */
    public void setInts(int val, int bitIndex, int bitWidth, int numRepeats) {
        int byteOffset = bitIndex >> 3;
        int bitOffset = bitIndex & 7;

        for (int n = 0; n < numRepeats; ++n) {
            int bitsPut = 0;
            int putVal = val;
            bb.put(byteOffset, (byte)((bb.get(byteOffset) & ((1 << bitOffset)-1)) + (putVal << bitOffset)));
            bitsPut += (8-bitOffset);
            ++byteOffset;
            putVal = putVal >> (8 - bitOffset);

            int remainingBits = bitWidth - bitsPut;
            while (remainingBits > 0) {
                bitOffset = 0;
                bb.put(byteOffset, (byte)((bb.get(byteOffset) & (0xFF << remainingBits)) + putVal));
                bitsPut += 8;
                remainingBits = bitWidth - bitsPut;
                putVal = putVal >> 8;
                if (remainingBits < 0) {
                    bitOffset = 8 + remainingBits;
                } else
                    ++byteOffset;
            }
        }
    }

    /**
     * Get the underlying ByteBuffer
     * @return the ByteBuffer that is used internally
     */
    public ByteBuffer getBytes() {
        return this.bb;
    }
}
