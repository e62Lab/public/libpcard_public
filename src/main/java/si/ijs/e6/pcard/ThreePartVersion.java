package si.ijs.e6.pcard;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by matjaz on 8/31/18.
 * 
 * Note on logger:
 *  SEVERE (highest value)
 *  WARNING
 *  INFO
 *  CONFIG
 *  FINE
 *  FINER
 *  FINEST (lowest value)
 */

/**
 * This class provide the basic facilities for reading and checking versions of type X.Y.Z
 *
 * All PCARD software is versioned with this version type (some extended, e.g. X.Y.Z githash (branch))
 */
public class ThreePartVersion {
    public static int INVALID_NUMBER = -1;
    private static final Logger logger = Logger.getLogger(ThreePartVersion.class.getName());
    
    // raw string that was provided to this class initializer
    private String raw;
    // for the assessment of features, the integer triplet is used; allow external access to these
    private int minor, major, patch;

    /**
     * Construct ThreePartVersion instance from the raw string.
     * @param version the revision string (pass it null or empty string to leave it uninitialized)
     */
    public ThreePartVersion(String version) {
        if (version == null)
            version = "";

        raw = version;
        if (!version.equals("N/A") && !version.equals(""))
            parseRawString(version);
    }
    
    /**
     * Basic converter to String. Returns all the firmware version blocks in human readable form.
     * @return the version string in human readable form
     */
    @Override
    public String toString() {
        return ""+major+"."+minor+"."+patch;
    }

    /**
     * Copy constructor.
     * @param other the instance to copy data from
     */
    public ThreePartVersion(ThreePartVersion other) {
        if (other == null) {
            raw = "N/A";
            major = minor = patch = 0;
        } else {
            raw = other.raw;
            major = other.major;
            minor = other.minor;
            patch = other.patch;
        }
    }

    /**
     * Get the raw string of the version (as initialized)
     *
     * @return the raw firmware version string
     */
    public String getRawString() {
        return raw;
    }

    /**
     * Test if the parsed version is properly formed. This can be used to detect errors in reading the raw string.
     * Note: parseRawString must be called first (can be done through the constructor)
     *
     * @return true if the version string is properly formed
     */
    public boolean isProperlyFormed() {
        return (INVALID_NUMBER != major) && (INVALID_NUMBER != minor) && (INVALID_NUMBER != patch);
    }

    /**
     * Test weather the version (major.minor.patch) is higher or equal than the one pprovidet to the function
     * @param major the major version number (MAJOR.minor.patch)
     * @param minor the minor version number (major.MINOR.patch)
     * @param patch the patch version number (major.minor.PATCH)
     * @return true if the stored version is at least as high as the numbers provided
     */
    public boolean minVersion(int major, int minor, int patch) {
        return (
                (this.major > major) ||
                        ((this.major == major) && ((this.minor > minor) || ((this.minor == minor) && (this.patch >= patch))))
        );
    }

    /**
     * Parse the string, which was read from the gadget; major.minor.patch is parsed as well as hardware revision and git commit hash
     * Note - parsing will first remove all non numeric characters and dots and then parse version; at least one number
     *  must be available for parsing to succeed
     *
     * @param raw raw string to parse
     */
    private void parseRawString(String raw) {
        try {
            if (!raw.equals("")) {
                String[] dotSplit = raw.replaceAll("[^0-9.]", "").split("\\.", 3);
                // setup minimal version before parsing the string
                major = minor = patch = INVALID_NUMBER;
                try {
                    major = Integer.parseInt(dotSplit[0]);
                    minor = (dotSplit.length > 1 ? Integer.parseInt(dotSplit[1]) : 0);
                    patch = (dotSplit.length > 2 ? Integer.parseInt(dotSplit[2]) : 0);
                } catch (Exception e) {
                    logger.info("Error in parsing the version \""+raw+"\", strings separated by dots are not all numbers.");
                }
            }
        } catch (Exception e) {
            logger.log(Level.WARNING, "Unexpected exception while parsing raw version string", e);
        }
    }

    /**
     * Implements a bug fix for one version of MobECG, which stored patch part of versions wrong.
     * MobECG 1.8.1 development version produced 1.0.2134 instead of 1.0.2 for some time
     */
    public void applyMobECGPatch() {
        // bug fix for MobECG
        if ((major == 1) && (minor == 0) && (patch == 2134)) {
            patch = 2;
            raw = raw.replaceFirst("2134", "2");
        }
    }

    /**
     * Get the first number of the three, aka the major version
     * @return the major version value
     */
    public int getValueOfMajor() {
        return major;
    }

    /**
     * Get the second number of the three, aka the minor version
     * @return the minor version value
     */
    public int getValueOfMinor() {
        return minor;
    }

    /**
     * Get the third number of the three, aka the patch version
     * @return the patch value
     */
    public int getValueOfPatch() {
        return patch;
    }
}
