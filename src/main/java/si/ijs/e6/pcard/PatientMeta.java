package si.ijs.e6.pcard;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Map;

/**
 * Class containing patient metadata, as it is entered in the form presented on the start of the
 * measurement
 */
public class PatientMeta {
    /**
     * Either a name or some kind of ID - a unique identifier for the person
     */
    public String nameOrId;
    /**
     * timestamp [milliseconds]
     * To get a date, do: Calendar a = new GregorianCalendar(); a.setTimeInMillis(birthdayTimestamp);
     */
    public long birthdayTimestamp;
    /**
     * Weight of the person [kg].
     */
    public float weight;
    /**
     * Sex / gender of the person
     */
    public Sex sex;
    /**
     * position and orientation of the ECG gadget on the body
     */
    public String ecgPositionString;
    /**
     * Miscellaneous data that might be optionally added to a dialog
     */
    public Map<String, String> optionalKeyValuePairs;
    /**
     * Length of the reported ECG interval [s]
     * While this is aot a patient metadata, it is more often than not included in the dialog.
     */
    public int reportIntervalLength;

    public PatientMeta() {
        // empty constructor
    }

    /**
     * Enum for Sex
     */
    public enum Sex {
        UNKNOWN(-1),
        MALE(0),
        FEMALE(1);

        private final int id;

        Sex(int id) {
            this.id = id;
        }

        public static Sex fromRadioSelect(boolean isMale, boolean isFemale) {
            if (isMale)
                return MALE;
            if (isFemale)
                return FEMALE;
            return UNKNOWN;
        }

        public static Sex toStringByInt(int id) {
            if (id == 0)
                return MALE;
            if (id == 1)
                return FEMALE;
            return UNKNOWN;
        }

        public static Sex fromString(String str) {
            if (str.equals(MALE.toString()))
                return MALE;
            if (str.equals(FEMALE.toString()))
                return FEMALE;
            return UNKNOWN;
        }

        public int getId() {
            return id;
        }
    }

    public static Calendar millisecondTimestampAsCalendar(long ts) {
        Calendar a = new GregorianCalendar();
        a.setTimeInMillis(ts);
        return a;
    }

    public static String millisecondTimestampAsString(long ts) {
        Date date = millisecondTimestampAsCalendar(ts).getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(date);
    }

    public static long millisecondTimestampFromString(String dateStr) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date;
        try {
            date = sdf.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
            return 0;
        }
        return date.getTime();
    }

    public String toString() {
        String ret = "{";
        ret += "\"name\": \""+nameOrId+"\", ";
        ret += "\"sex\": \""+sex.toString()+"\", ";
        ret += "\"birth date\": \""+ millisecondTimestampAsString(birthdayTimestamp)+"\", ";
        ret += "\"birth date timestamp\": \""+birthdayTimestamp+"\", ";
        ret += "\"weight\": \""+weight+"\",";
        ret += "\"report_interval_len\": \""+reportIntervalLength+"\"";
        return ret;
    }
}
