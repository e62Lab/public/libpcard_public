package si.ijs.e6.pcard;

import static si.ijs.e6.pcard.Pcard.DEFAULT_RECORDING_APP_NAME;


/**
 * This class is used when loading files, e.g. s2 file with raw measurement, to decode the
 * description string of the recording software
 *
 * The form of recognised string:
 * <ul>
 *     <li>
 *         "MobECG 1.8.1-69-g616eabfa (master)"
 *     </li>
 * </ul>
 */
public class RecordingSoftwareDescription {
    String softwareBaseName = "";
    ThreePartVersion softwareVersion = new ThreePartVersion("");
    String additionalDescription = "";
    /**
     * Factory method for parsing a given string
     * @param value a string-encoded software description consisting of Software _space_ ThreePartVersion _more_sub-version_strings_
     *              Aa example is "MobECG 1.8.1-69-g616eabfa (master)", which is constructed as
     *              "MobECG "+context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
     * @return a new object instance (this is a factory method)
     */
    public static RecordingSoftwareDescription parseString(String value) {
        RecordingSoftwareDescription rsd = new RecordingSoftwareDescription();
        // split the input string by spaces and decide if known hardware, based on the first entry
        String[] firstValueSplit = value.split("\\s", 2);

        {
            rsd.softwareBaseName = firstValueSplit[0];

            // Only MobECG and its quirks are recognised at this point
            if (firstValueSplit[0].equalsIgnoreCase(DEFAULT_RECORDING_APP_NAME)) {
                // split the second part further
                String[] split = firstValueSplit[1].split("-", 2);
                rsd.softwareVersion = new ThreePartVersion(split[0]);
                rsd.additionalDescription = split[1];
            }
        }
        return rsd;
    }

    /**
     * Get the 'software version' part of the description, e.g. 1.2.3
     * @return the software version in form of {@link ThreePartVersion}
     */
    public ThreePartVersion getVersion() {
        return softwareVersion;
    }

    /**
     * Get the software name
     * @return the name string
     */
    public String getName() {
        return softwareBaseName;
    }

    /**
     * Get the additional description (such as git hash and/or branch name)
     * @return
     */
    public String getAdditionalDescription() {
        return additionalDescription;
    }

    /**
     * Convert the parsed description to a string, which is again parsable
     * @return software description as a string
     */
    public String toString() {
        return getName() + " " + getVersion().toString() + " " + getAdditionalDescription();
    }
}
