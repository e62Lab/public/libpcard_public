# PCARD java library

This repository contains basic PCARD constants and definitions that can be used in implementations of PCARD Wireless Protocol, PCARD version of s2 files and all other PCARD related code.

## The provided parts (classes, constants, etc)

### class LinearTransformationFunction

This class provides the encapsulation of the transformation function that should be applied on the raw samples to convert them to mV. Calibration constants are loaded directly to this function

### class FirmwareRevision

Encapsulates the formatting of firmware revision string of the PCARD devices (including Savvy versions).
It also enables simple checking for functionalities that are revision dependant.

This class is extended from class ThreePartVersion, which can be reused to implement versioning with schema major.minor.revision

### class UnpackedEcgData

A generic class that holds one packet of ECG data that was sampled with PCARD device.

### classes  DataStreamPacket and DataStreamProtocol

These two classes encapsulate the data stream packets as found in PWP (and then also used in s2); UnpackedEcgData is also used by these two classes.

### class MultiBitBuffer

Helper class for packing and unpacking bitfields (used in DataStreamPacket)
 
